const assert = require("assert");

describe("screenshot comparison tests", () => {
  it("should save homepage screenshot", () => {
    browser.url("/");
    browser.saveFullPageScreen("fullPage");
  });

  it("should compare successful with a baseline", () => {
    browser.url("/");
    assert.ok(browser.checkFullPageScreen("fullPage") == 0);
  });
});
