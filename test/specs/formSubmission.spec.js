const assert = require("assert");
const { debug } = require("console");
const baseUrl = browser.options.baseUrl;

describe("form submission tests", () => {
  beforeEach(function () {
    browser.url("/demo");
  });

  const fieldVals = {
    firstName: "Michael",
    lastName: "Scott",
    email: "mscott@office.com",
    companyName: "Dunder Mifflin",
    companyType: "Paper",
    phone: 888888,
  };

  function populate() {
    const firstName_field = $("#firstName");
    const lastName_field = $("#lastName");
    const email_field = $("#email");
    const companyName_field = $("#companyName");
    const companyType_field = $("#companyType");
    const phone_field = $("#phone");
    firstName_field.setValue(fieldVals.firstName);
    lastName_field.setValue(fieldVals.lastName);
    email_field.setValue(fieldVals.email);
    companyName_field.setValue(fieldVals.companyName);
    companyType_field.setValue(fieldVals.companyType);
    phone_field.setValue(fieldVals.phone);
  }

  var fieldNames = [
    "First Name",
    "Last Name",
    "Email",
    "Company Name",
    "Company Type",
    "Phone",
  ];

  /* ########## Check If Objects Exist ########## */
  //it should check if First Name input field exists
  it("should check if First Name input field exists", () => {
    const firstName_field = $("#firstName");
    assert.ok(firstName_field.isExisting(), "First name field does not exist");
  });
  //it should check if Last Name input field exists
  it("should check if Last Name input field exists", () => {
    const lastName_field = $("#lastName");
    assert.ok(lastName_field.isExisting(), "Last name field does not exist");
  });
  //it should check if Email input field exists
  it("should check if Email input field exists", () => {
    const email_field = $("#email");
    assert.ok(email_field.isExisting(), "Email field does not exist");
  });
  //it should check if Company Name input field exists
  it("should check if Company Name input field exists", () => {
    const companyName_field = $("#companyName");
    assert.ok(
      companyName_field.isExisting(),
      "Company Name field does not exist"
    );
  });
  //it should check if Company Type input field exists
  it("should check if Company Type input field exists", () => {
    const companyType_field = $("#companyType");
    assert.ok(
      companyType_field.isExisting(),
      "Company Type field does not exist"
    );
  });
  //it should check if Phone input field exists
  it("should check if Phone input field exists", () => {
    const phone_field = $("#phone");
    assert.ok(phone_field.isExisting(), "Phone field does not exist");
  });

  /* ########## Check If Object inputs are empty ########## */

  //it should check if First Name input is empty
  it("should check if First Name input is empty", () => {
    fieldVals.firstName = "";
    populate();
    const submitButton = $(
      '//form[@class="demo-form"]/button[text()="Submit"]'
    );
    submitButton.click();
    const firstName_error = $(
      '//label[text()="First Name"]/small[@class="error-msg"]'
    );
    browser.pause(3000);
    // browser.debug();
    assert.ok(firstName_error.isExisting(), "Error message is not displayed");
    const URL = browser.getUrl();
    assert.equal(URL, `${baseUrl}/demo`);
  });

  //it should check if Last Name input is empty
  it("should check if Last Name input is empty", () => {
    fieldVals.firstName = "Jim";
    fieldVals.lastName = "";
    populate();
    const submitButton = $(
      '//form[@class="demo-form"]/button[text()="Submit"]'
    );
    submitButton.click();
    const lastName_error = $(
      '//label[text()="Last Name"]/small[@class="error-msg"]'
    );
    browser.pause(3000);
    // browser.debug();
    assert.ok(lastName_error.isExisting(), "Error message is not displayed");
    const URL = browser.getUrl();
    assert.equal(URL, `${baseUrl}/demo`);
  });

  //it should check if Email input is empty
  it("should check if Email input is empty", () => {
    fieldVals.lastName = "Halpert";
    fieldVals.email = "";
    populate();
    const submitButton = $(
      '//form[@class="demo-form"]/button[text()="Submit"]'
    );
    submitButton.click();
    const email_error = $('//label[text()="Email"]/small[@class="error-msg"]');
    browser.pause(3000);
    // browser.debug();
    assert.ok(email_error.isExisting(), "Error message is not displayed");
    const URL = browser.getUrl();
    assert.equal(URL, `${baseUrl}/demo`);
  });

  //it should check if Company Name input is empty
  it("should check if Company Name input is empty", () => {
    fieldVals.email = "jhalpert@office.com";
    fieldVals.companyName = "";
    populate();
    const submitButton = $(
      '//form[@class="demo-form"]/button[text()="Submit"]'
    );
    submitButton.click();
    const companyName_error = $(
      '//label[text()="Company Name"]/small[@class="error-msg"]'
    );
    browser.pause(3000);
    // browser.debug();
    assert.ok(companyName_error.isExisting(), "Error message is not displayed");
    const URL = browser.getUrl();
    assert.equal(URL, `${baseUrl}/demo`);
  });

  //it should check if Company Type input is empty
  it("should check if Company Type input is empty", () => {
    fieldVals.companyName = "Athlead";
    fieldVals.companyType = "";
    populate();
    const submitButton = $(
      '//form[@class="demo-form"]/button[text()="Submit"]'
    );
    submitButton.click();
    const companyType_error = $(
      '//label[text()="Company Type"]/small[@class="error-msg"]'
    );
    browser.pause(3000);
    // browser.debug();
    assert.ok(companyType_error.isExisting(), "Error message is not displayed");
    const URL = browser.getUrl();
    assert.equal(URL, `${baseUrl}/demo`);
  });

  //it should check if Phone input is empty
  it("should check if Phone input is empty", () => {
    fieldVals.companyType = "Sports Marketing";
    fieldVals.phone = "";
    populate();
    const submitButton = $(
      '//form[@class="demo-form"]/button[text()="Submit"]'
    );
    submitButton.click();
    const phone_error = $('//label[text()="Phone"]/small[@class="error-msg"]');
    browser.pause(3000);
    // browser.debug();
    assert.ok(phone_error.isExisting(), "Error message is not displayed");
    const URL = browser.getUrl();
    assert.equal(URL, `${baseUrl}/demo`);
    fieldVals.phone = 111111;
  });

  //it should check if ALL fields are empty
  it(`should check if ALL fields are empty`, () => {
    const submitButton = $(
      '//form[@class="demo-form"]/button[text()="Submit"]'
    );
    submitButton.click();
    browser.pause(3000);
    fieldNames.forEach(function (item) {
      const error_msg = $(
        `//label[text()="${item}"]/small[@class="error-msg"]`
      );
      assert.ok(
        error_msg.isExisting(),
        `Error message for ${item} is not displayed`
      );
      const URL = browser.getUrl();
      assert.equal(URL, `${baseUrl}/demo`);
    });
  });

  /* ########## Thank you page ########## */
  //it should check if Thank You page redirection is correct
  it("should check if Thank You page redirection is correct", () => {
    populate();
    const submitButton = $(
      '//form[@class="demo-form"]/button[text()="Submit"]'
    );
    submitButton.click();
    // browser.debug();
    browser.pause(3000);
    var thankYouHeader = $("h1.title=Thank You");
    assert.ok(thankYouHeader.isExisting(), "Thank You page is not displayed");
    const URL = browser.getUrl();
    assert.equal(URL, `${baseUrl}/thanks`);
  });
});
