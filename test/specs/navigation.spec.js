const assert = require("assert");
const baseUrl = browser.options.baseUrl;

describe("navigation tests", () => {
  beforeEach(function () {
    browser.url("/");
  });
  it("loads the homepage", () => {
    var title = browser.getTitle();
    assert.equal(title, "React App");
    const URL = browser.getUrl();
    assert.equal(URL, `${baseUrl}/`);
  });
  /* ########## Home Page ########## */
  // it should check if Home button exists
  it("should check if Home button exists", () => {
    var homeButton = $(".navbar-item=Home");
    assert.ok(homeButton.isExisting(), "Home button does not exist");
  });
  // it should check if Home button redirection is correct
  it("should check if Home button redirection is correct", () => {
    var homeButton = $(".navbar-item=Home");
    var homeHeader = $("h1.title=Home Page");
    homeButton.click();
    // browser.debug();
    assert.ok(homeHeader.isExisting(), "Home Page is not displayed");
    const URL = browser.getUrl();
    assert.equal(URL, `${baseUrl}/`);
  });

  /* ########## About Page ########## */
  // it should check if About button exists
  it("should check if About button exists", () => {
    var aboutButton = $(".navbar-item=About");
    assert.ok(aboutButton.isExisting(), "About button does not exist");
  });
  // it should check if About button redirection is correct
  it("should check if About button redirection is correct", () => {
    var aboutButton = $(".navbar-item=About");
    var aboutHeader = $("h1.title=About Us");
    aboutButton.click();
    // browser.debug();
    assert.ok(aboutHeader.isExisting(), "About Page is not displayed");
    const URL = browser.getUrl();
    assert.equal(URL, `${baseUrl}/about`);
  });

  /* ########## Demo Page ########## */
  // it should check if Demo button exists
  it("should check if Demo button exists", () => {
    var demoButton = $('//div[@class="buttons"]/a[text()="Request a Demo"]');
    assert.ok(demoButton.isExisting(), "Request a Demo button does not exist");
  });
  // it should check if Demo button redirection is correct
  it("should check if Demo button redirection is correct", () => {
    var demoButton = $(
      '//nav/div/div[@class="buttons"]/a[text()="Request a Demo"]'
    );
    var demoHeader = $("h1.title=Contact Us");
    demoButton.click();
    // browser.debug();
    assert.ok(demoHeader.isExisting(), "Contact Us Page is not displayed");
    const URL = browser.getUrl();
    assert.equal(URL, `${baseUrl}/demo`);
  });
});
